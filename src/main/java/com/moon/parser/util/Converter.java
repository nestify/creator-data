package com.moon.parser.util;

import com.moon.parser.model.GeoPoints.GeoPoints;
import lombok.AllArgsConstructor;

import lombok.extern.log4j.Log4j2;
import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.util.Timeout;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UriUtils;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


@AllArgsConstructor

public class Converter {
	public static void main(String[] args) {
//		GeoPoints coordinates = Converter.getCoordinates("Dubina 521, Hranovnica, okres Poprad");
		GeoPoints coordinates = Converter.getCoordinates("Prešovská 40/B, Bratislava-Ružinov, okres Bratislava II");

		System.out.println("coordinates = " + coordinates);
	}

	public static GeoPoints getCoordinates(String address) {

		RequestConfig config = RequestConfig.custom()
				.setConnectTimeout(Timeout.of(5, TimeUnit.SECONDS)) // Максимальний час очікування з'єднання
				.setConnectionRequestTimeout(Timeout.of(5, TimeUnit.SECONDS)) // Максимальний час очікування отримання з'єднання з пулу
				.build();

		HttpClient httpClient = HttpClients.custom()
				.setDefaultRequestConfig(config)
				.build();

		RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient));
		String replaceAddress = address.replace("ulica", "").replace("Ulica", "");
		address = replaceAddress;
		if (replaceAddress.contains("/")) {
			int i = replaceAddress.indexOf("/");
			if (i < replaceAddress.length() - 2) {
				char charAfterSlash = replaceAddress.charAt(i + 1);
				if (!Character.isDigit(charAfterSlash)) {
					String firstPart = replaceAddress.substring(0, i);
					int comaIndex = replaceAddress.indexOf(",");
					String secondPart = replaceAddress.substring(comaIndex);
					address = firstPart + secondPart;
				} else {
					String substring = replaceAddress.substring(0, i);
					int firstSpaceBefore = substring.lastIndexOf(" ");
					String firstPart = replaceAddress.substring(0, firstSpaceBefore).split(" ")[0];
					String secondPart = replaceAddress.substring(i).replace("/", " ");
					address = firstPart + secondPart;
				}

			}
		}
		String encodedAddress = UriUtils.encodeQueryParam(address, "UTF-8");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("User-Agent", "Spring 5 RestTemplate");


		URI uri = UriComponentsBuilder
				.fromUriString("https://nominatim.openstreetmap.org/search")
				.queryParam("q", encodedAddress)
				.queryParam("format", "json")
				.build(true)
				.toUri();

		HttpEntity<String> entity = new HttpEntity<>("parameters", httpHeaders);
		Map<String, Object>[] response = restTemplate.exchange(uri, HttpMethod.GET, entity, Map[].class).getBody();
		if (response != null && response.length > 0) {
			Map<String, Object> location = response[0];
			return new GeoPoints(location.get("lat").toString(), location.get("lon").toString());
		}
		return null;
	}
}

