package com.moon.parser.repositoty;

import com.moon.parser.model.Advert;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AdvertRepository extends MongoRepository<Advert, String> {
	boolean existsAdvertByLink(String link);
	void deleteAdvertsByLinkIn(List<String> list);
	Optional<Advert> getAdvertByTitle(String title);
}

