package com.moon.parser.parser;

import com.moon.parser.model.Advert;
import com.moon.parser.service.AdvertService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@AllArgsConstructor
@Slf4j
public class ParserScheduler {
	private final AdvertService advertService;
	private final int maxRetries = 3;

	@Scheduled(cron = "0 13 */2 * * *")
	public void parseNewAdvert() {

		log.info("Starting parse at {}", getCurrentTime());
		for (int page = 0; page < 450; page++) {
			String url = buildPageUrl(page);
			int retries = 0;

			while (retries < maxRetries) {
				try {
					Document document = fetchDocument(url);
					parseAndSaveAdverts(document);
					break;
				} catch (IOException e) {
					log.warn("Failed to fetch data from URL: " + url);
					retries++;
				}
			}
		}
		log.info("End parse at {}", getCurrentTime());
	}

	private static String getCurrentTime() {
		Date currentDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(currentDate);
	}

	private String buildPageUrl(int page) {
		return "https://www.nehnutelnosti.sk/prenajom/?p[page]=" + page;
	}

	private Document fetchDocument(String url) throws IOException {
		return Jsoup.connect(url)
				.referrer("https://www.google.com/")
				.timeout(7000)
				.get();
	}

	private void parseAndSaveAdverts(Document document) throws IOException {
		List<Advert> addToDatabase = new LinkedList<>();
		Elements advertLinks = document.select(".advertisement-item--content__title.d-block.text-truncate");
		for (Element link : advertLinks) {
			String linkText = link.attr("href");
			Advert advert = ParseOperation.parseAdvert(linkText);
			if(advert!=null) {
				addToDatabase.add(advert);
			}
		}
		saveAllAdvertsToDatabase(addToDatabase);
	}


	@Scheduled(cron = "0 18 */2 * * *")
	public void checkIfLinkIsStillInUse() throws IOException {
		log.info("Starting cleaning at {}", getCurrentTime());
		List<String> advertsNotExist = new LinkedList<>();
		for (int page = 0; page < 450; page++) {
			String url = "https://www.nehnutelnosti.sk/prenajom/?p[page]=" + page;
			Document document = Jsoup.connect(url)
					.referrer("https://www.google.com/")
					.timeout(5000)
					.get();
			Elements links = document.getElementsByTag("a");
			for (Element link : links) {
				if (link.hasClass("advertisement-item--content__title d-block text-truncate")) {
					String linkText = link.attr("href");
					if (!advertService.isAdvertExistByLink(linkText)) {
						advertsNotExist.add(linkText);
					}
				}
			}
			deleteAdvertFromMongo(advertsNotExist);
		}
		log.info("End cleaning at {}", getCurrentTime());
	}

	private void saveAllAdvertsToDatabase(List<Advert> advertsToSaveInDatabase) {
		advertsToSaveInDatabase.removeIf(advert -> advertService.isAdvertExistByLink(advert.getLink()));
		advertService.saveAllAdverts(advertsToSaveInDatabase);
	}

	private void deleteAdvertFromMongo(List<String> allAdvertFromDatabase) {
		advertService.deleteAllAdvertsByLink(allAdvertFromDatabase);
	}
}
