package com.moon.parser.parser;

import com.moon.parser.model.Advert;
import com.moon.parser.model.GeoPoints.GeoPoints;
import com.moon.parser.util.Converter;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ParseOperation {

	public static Advert parseAdvert(String linkText) throws IOException {
		Document document = Jsoup.connect(linkText)
				.referrer("https://www.google.com/")
				.get();
		String redirectedLink = getRedirectedLink(linkText);
		Document imagesDocument = Jsoup.connect(redirectedLink.replace("/detail/", "/detail/galeria/"))
				.referrer("https://www.google.com/")
				.get();
		Elements imagesElements = imagesDocument.getElementsByTag("img");
		Elements elementsPrice = document.getElementsByClass("MuiTypography-root MuiTypography-h3 css-wupcfx");
		Elements elementsTitle = document.getElementsByTag("h1");
		Elements elementsLocation = document.getElementsByClass("MuiTypography-root MuiTypography-body2 MuiTypography-noWrap css-1j9asos");
		Elements allH2 = document.getElementsByTag("h2");
		Element elementDescription = document.selectFirst("#hidden-text-description");
		Elements ownershipElements = document.getElementsByClass("MuiTypography-root MuiTypography-label2 css-7oq8oe");
		Elements ownershipNameElements = document.getElementsByClass("MuiTypography-root MuiTypography-h5 css-1akkows");
		Elements realityAgencyNameElements = document.getElementsByClass("MuiBox-root css-1itv5e3");
		Elements elementsSquare = document.getElementsByClass("MuiStack-root css-hismlq");
		Elements fullAddressElement = document.getElementsByClass("MuiTypography-root MuiTypography-body2 MuiTypography-noWrap css-1j9asos");
		String fullAddress = parseFullAddress(fullAddressElement);
		log.info(fullAddress);
		String typeBuilding = parseTypeBuilding(elementsSquare);
		String title = parseTitle(elementsTitle);
		String location = parseLocation(elementsLocation);
		String street = parseStreet(elementsLocation);
		String square = parseSquare(elementsSquare);
		String countRoom = parseCountRoom(allH2);
		String typeOperation = parseTypeOperation(allH2);
		String description = parseDescription(elementDescription);
		List<String> images = parseMainImage(imagesElements);
		String ownership = parseOwnership(ownershipElements);
		String ownershipNames = parseOwnershipNames(ownershipNameElements);
		String withChild = parseChild(elementDescription);
		String realityAgencyName = parseRealityAgencyName(realityAgencyNameElements);
		String price = parsePrice(elementsPrice);
		String allowPet = parsePet(elementDescription);
		log.info("Start coordinates");
		GeoPoints coordinates = Converter.getCoordinates(fullAddress);
		log.info("coordinates {}", coordinates);
		if (
				price != null
						&& title != null
						&& location != null
						&& street != null
						&& square != null
						&& countRoom != null
						&& typeOperation != null
						&& description != null
						&& ownership != null
						&& !images.isEmpty()
						&& ownershipNames != null
						&& typeBuilding != null
		) {
			return Advert.builder()
					.title(title)
					.location(location)
					.price(price)
					.square(square)
					.countRoom(countRoom)
					.typeOperation(typeOperation)
					.street(street)
					.description(description)
					.link(linkText)
					.images(images)
					.ownership(ownership)
					.ownershipNames(ownershipNames)
					.withChild(withChild)
					.geoPoints(coordinates)
					.allowPet(allowPet)
					.realityAgencyName(realityAgencyName)
					.typeBuilding(typeBuilding)
					.fullAddress(fullAddress)
					.createdByUserId("parser")
					.build();
		}
		return null;
	}

	private static String parseFullAddress(Elements fullAddressElement) {
		return fullAddressElement.get(0).text();
	}

	private static String getRedirectedLink(String link) {
		WebDriver driver;
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		options.addArguments("--disable-gpu");
		driver = new ChromeDriver(options);
		try {
			driver.get(link.replace("https://www.nehnutelnosti.sk/", "https://www.nehnutelnosti.sk/detail/"));
			return driver.getCurrentUrl();
		} finally {
			driver.quit();
		}
	}


	private static String parseTypeBuilding(Elements elementsSquare) {
		for (Element element : elementsSquare) {
			boolean isItSquareAndHouse = element.text().contains("m²") && element.text().contains("Plocha domu: ");
			boolean isItSquareAndFlat = element.text().contains("m²") && element.text().contains("Plocha bytu: ");
			if (isItSquareAndHouse) {
				return "dom";
			} else if (isItSquareAndFlat) {
				return "byt";
			}
		}
		return null;
	}

	private static String parseRealityAgencyName(Elements realityAgencyNameElements) {
		if (!realityAgencyNameElements.isEmpty()) {
			if (realityAgencyNameElements.get(0).childrenSize() > 0) {
				return realityAgencyNameElements.get(0).child(0).text().trim();
			}
		}
		return "noname";
	}

	private static String parseChild(Element elementDescription) {
		if (elementDescription != null) {
			if (elementDescription.text().contains("s detmi")) {
				return "yes";
			}
		}
		return "don't know";
	}

	private static String parsePet(Element elementDescription) {

		if (elementDescription != null) {
			String text = elementDescription.text();
			if (text.contains("zvierata su povolene")
					|| text.contains("Zvieratká povolené")
					|| text.contains("zvieratká povolené")
					|| text.contains("zvieratka povolene")
					|| text.contains("zvierata povolene")
					|| text.contains("zviera je tiež povolené")
					|| text.contains("zviera je povolené")
					|| text.contains("Zvieratá povolené")) {
				return "yes";
			} else if (
					text.contains("Zvieratá nie sú povolené")
							|| text.contains("niesú povolené domáce zvieratá")
							|| text.contains("zvieratá nie sú povolené")
			) return "no";
		}
		return "don't know";
	}

	private static String parseOwnershipNames(Elements ownershipNameElements) {

		for (Element element : ownershipNameElements) {
			String ownerName = element.text();
			if (!ownerName.isEmpty()) {
				return ownerName.trim();
			}
		}
		return null;
	}

	private static String parseOwnership(Elements ownershipElements) {
		if (ownershipElements != null) {
			for (Element element : ownershipElements) {
				String owner = element.text();
				if (owner.contains("SÚKROMNÁ OSOBA") || owner.contains("SUKROMNA OSOBA")) {
					return "owner";
				}
			}
			return "non-owner";
		}
		return null;
	}

	private static List<String> parseMainImage(Elements elementsImage) {
		List<String> images = new LinkedList<>();
		for (Element element : elementsImage) {
			if (!element.attr("src").contains("https://img.unitedclassifieds.sk/")) {
				continue;
			}
			String src = element.attr("src");
			images.add(src);
		}
		return images;
	}


	private static String parseDescription(Element description) {
		if (description != null && description.text().length() < 1999) {
			return description.text();
		} else if (description != null && description.text().length() > 1999) {
			return description.text()
					.substring(0, 1999)
					.replace("<br />", "")
					.replace("<br />", "");
		}
		return null;
	}

	private static String parseTypeOperation(Elements allH2) {
		for (Element element : allH2) {
			boolean isItRoom = element.hasClass("MuiTypography-root MuiTypography-h4 MuiTypography-noWrap css-16vsc38");
			if (isItRoom) {
				if (element.text().contains("prenájom") || element.text().contains("prenajom")) {
					return "prenájom";
				}
			}
		}
		return null;
	}

	private static String parseCountRoom(Elements elementsCountRoom) {
		for (Element element : elementsCountRoom) {
			boolean isItRoom = element.hasClass("MuiTypography-root MuiTypography-h4 MuiTypography-noWrap css-16vsc38");
			if (isItRoom) {
				if (!element.text().contains("Ro")) {
					return element.text().substring(0, 2).trim();
				} else
					return "0";
			}
		}
		return null;
	}

	private static String parseSquare(Elements elementsSquare) {
		for (Element element : elementsSquare) {
			boolean isItSquare = element.text().contains("m²");
			if (isItSquare) {
				return element.text()
						.replace("Plocha bytu: ", "")
						.replace("m²", "")
						.replace("Plocha domu: ", "").trim();
			}
		}
		return null;
	}

	private static String parseLocation(Elements elementsLocation) {
		for (Element element : elementsLocation) {
			boolean isItLocation = element.parent().hasClass("MuiStack-root css-1r9kwv0");
			if (isItLocation) {
				String location = element.text();
				if (location.contains("okres")) {
					return location.substring(location.lastIndexOf("okres")).replace("okres", "").trim();
				}
			}
		}
		return null;
	}

	private static String parseStreet(Elements elementsLocation) {
		for (Element element : elementsLocation) {
			boolean isItLocation = element.parent().hasClass("MuiStack-root css-1r9kwv0");
			if (isItLocation) {
				String location = element.text();
				if (location.contains("okres")) {
					return location.substring(0, location.indexOf(",")).trim();
				}
			}
		}
		return null;
	}


	private static String parseTitle(Elements elementsTitle) {
		for (Element element : elementsTitle) {
			boolean isItTitle = element.parent().hasClass("MuiBox-root css-1qm1lh");
			if (isItTitle) {
				return element.text().trim();
			}
		}
		return null;
	}

	private static String parsePrice(Elements elementsPrice) {
		for (Element element : elementsPrice) {
			boolean isItPrice = element.parent().hasClass("MuiStack-root css-1kkt86i");
			if (isItPrice) {
				return element.text().replace("€/mes.", "")
						.replace("Cena dohodou", "0")
						.replace("€", "").trim();
			}
		}
		return null;
	}

}
