package com.moon.parser.notion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DatabaseModel {
	private String object;
	@JsonProperty("results")
	private List<Page> pages = new ArrayList<>();
	@JsonProperty("next_cursor")
	private Boolean nextCursor;
	@JsonProperty("has_more")
	private Boolean hasMore;
}
