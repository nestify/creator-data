package com.moon.parser.notion;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@ConfigurationProperties("notion")
public record NotionConfigProperties(String apiUrl,String apiVersion, String authToken, String databaseId) {
}
