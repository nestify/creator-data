package com.moon.parser.notion;

import com.moon.parser.model.Advert;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class DatabaseService {
	private final NotionConfigProperties configProperties;
	private final RestTemplate restTemplate;

	public void createAdvertInNotion(Advert advert) {
//		String url = configProperties.apiUrl() + "/v1/databases/" + databaseId + "/query"; - на get запит
		String url = "https://api.notion.com/v1/pages";
		RequestBody requestBODY = new RequestBody();
		requestBODY.setProperties(new NotionDatabaseService().create(advert));
		ResponseEntity<DatabaseModel> db = restTemplate.exchange(
				url,
				HttpMethod.POST,
				new HttpEntity<>(requestBODY, getDefaultHeaders()),
				DatabaseModel.class
		);
	}


	public String getAdvertLintNehnutelnostiById(int id) {
		String url = configProperties.apiUrl() + "/v1/databases/" + "4c9f611436254871b2f9f5db02bc43d7" + "/query";
		ResponseEntity<DatabaseModel> db = restTemplate.exchange(
				url,
				HttpMethod.POST,
				new HttpEntity<>(getDefaultHeaders()),
				DatabaseModel.class
		);
		String idOfPage = db.getBody().getPages().get(id).getId();
		System.out.println(idOfPage);

		String urlForUpdate = "https://api.notion.com/v1/pages/" + idOfPage;
//		RequestBody requestBODY = new RequestBody();
//		requestBODY.setProperties();


//		ResponseEntity<DatabaseModel> dbUpdate = restTemplate.exchange(
//				urlForUpdate,
//				HttpMethod.PATCH,
//				new HttpEntity<>(properties,getDefaultHeaders()),
//				DatabaseModel.class
//		);
		return db.getBody().getPages().get(id).getProperties().get("link").get("rich_text").get(0).get("plain_text").asText();
	}

	public List<Page> getAdvertsFromNotion2() {
		String url = configProperties.apiUrl() + "/v1/databases/" + configProperties.databaseId() + "/query";
		ResponseEntity<DatabaseModel> db = restTemplate.exchange(
				url,
				HttpMethod.POST,
				new HttpEntity<>(getDefaultHeaders()),
				DatabaseModel.class
		);
		return db.getBody().getPages();
	}

	private HttpHeaders getDefaultHeaders() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("Content-Type", "application/json");
		httpHeaders.set("accept", "application/json");
		httpHeaders.set("Notion-Version", configProperties.apiVersion());
		httpHeaders.set("Authorization", "Bearer " + configProperties.authToken());
		return httpHeaders;
	}
}
