package com.moon.parser.notion;

import com.moon.parser.model.Advert;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NotionDatabaseService {
	public Map<String, Map<String, List<Map<String, Object>>>> create(Advert advert) {
		Map<String, Map<String, List<Map<String, Object>>>> jsonObject = new HashMap<>();

		List<Map<String, Object>> title = createRichText(advert.getTitle());
		List<Map<String, Object>> price = createRichText(advert.getPrice());
		List<Map<String, Object>> location = createRichText(advert.getLocation());
		List<Map<String, Object>> street = createRichText(advert.getStreet());
		List<Map<String, Object>> square = createRichText(advert.getSquare());
		List<Map<String, Object>> countRoom = createRichText(advert.getCountRoom());
		List<Map<String, Object>> typeOperation = createRichText(advert.getTypeOperation());
		List<Map<String, Object>> description = createRichText(advert.getDescription());
		List<Map<String, Object>> link = createRichText(advert.getLink());
//		List<Map<String, Object>> images = createRichText(advert.getImages());
		List<Map<String, Object>> ownership = createRichText(advert.getOwnership());
		List<Map<String, Object>> ownershipNames = createRichText(advert.getOwnershipNames());
		List<Map<String, Object>> realityAgencyName = createRichText(advert.getRealityAgencyName());

		jsonObject.put("title", createRichTextMap(title));
		jsonObject.put("price", createRichTextMap(price));
		jsonObject.put("location", createRichTextMap(location));
		jsonObject.put("street", createRichTextMap(street));
		jsonObject.put("square", createRichTextMap(square));
		jsonObject.put("countRoom", createRichTextMap(countRoom));
		jsonObject.put("typeOperation", createRichTextMap(typeOperation));
		jsonObject.put("description", createRichTextMap(description));
		jsonObject.put("link", createRichTextMap(link));
//		jsonObject.put("images", createRichTextMap(images));
		jsonObject.put("ownership", createRichTextMap(ownership));
		jsonObject.put("ownershipNames", createRichTextMap(ownershipNames));
		jsonObject.put("realityAgencyName", createRichTextMap(realityAgencyName));

		return jsonObject;
	}

	private List<Map<String, Object>> createRichText(String content) {
		List<Map<String, Object>> richTextList = new ArrayList<>();

		Map<String, Object> textContent = new HashMap<>();
		textContent.put("content", content);

		Map<String, Object> textType = new HashMap<>();
		textType.put("text", textContent);
		textType.put("type", "text");

		richTextList.add(textType);

		return richTextList;
	}

	private Map<String, List<Map<String, Object>>> createRichTextMap(List<Map<String, Object>> richTextList) {
		Map<String, List<Map<String, Object>>> richTextMap = new HashMap<>();
		richTextMap.put("rich_text", richTextList);
		return richTextMap;
	}
}
