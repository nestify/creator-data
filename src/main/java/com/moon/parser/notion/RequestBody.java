package com.moon.parser.notion;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class RequestBody {
	private final Parent parent=new Parent();
	private Map<String, Map<String, List<Map<String, Object>>>> properties;
}
