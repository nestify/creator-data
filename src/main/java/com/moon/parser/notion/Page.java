package com.moon.parser.notion;



import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
public class Page {
	private String object;
	private String id;
	private JsonNode properties;
}
