package com.moon.parser.model;


import com.moon.parser.model.GeoPoints.GeoPoints;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.Generated;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Advert {
	@Id
	private String id;
	private String title;
	private String price;
	private String typeBuilding;
	private String location;
	private String street;
	private String fullAddress;
	private GeoPoints geoPoints;
	private String square;
	private String countRoom;
	private String typeOperation;
	private String description;
	private String link;
	private List<String> images;
	private String ownership;
	private String ownershipNames;
	private String withChild;
	private String allowPet;
	private String realityAgencyName;
	private String createdByUserId;
}
