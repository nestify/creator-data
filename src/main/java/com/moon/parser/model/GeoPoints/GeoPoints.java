package com.moon.parser.model.GeoPoints;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class GeoPoints {
	private String latitude;
	private String longitude;
}
