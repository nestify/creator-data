package com.moon.parser.service;

import com.mongodb.MongoException;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.moon.parser.model.Advert;
import com.moon.parser.repositoty.AdvertRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class AdvertService {
	private final AdvertRepository advertRepository;
	private final MongoClient mongoClient;

	@Transactional
	public boolean isAdvertExistByLink(String link) {
		try (ClientSession session = mongoClient.startSession()) {
			session.startTransaction();
			session.commitTransaction();
			return advertRepository.existsAdvertByLink(link);
		} catch (MongoException e) {
			e.printStackTrace();
		}
		return false;
	}


	@Transactional
	public void saveAllAdverts(List<Advert> adverts) {
		try (ClientSession session = mongoClient.startSession()) {
			session.startTransaction();
			advertRepository.saveAll(adverts);
			session.commitTransaction();
		} catch (MongoException e) {
			e.printStackTrace();
		}
	}

	@Transactional
	public void deleteAllAdvertsByLink(List<String> links) {
		advertRepository.deleteAdvertsByLinkIn(links);
	}

}
