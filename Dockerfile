FROM openjdk:latest
LABEL authors="orestklymko"
LABEL image_name="my-parser-image"
ADD target/parser-0.0.1-SNAPSHOT.jar parser.jar
ENTRYPOINT ["java", "-jar","parser.jar"]
