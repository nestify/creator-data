mvn install -DskipTests
docker build -t my-parser-image .
docker tag my-parser-image orestklymko/my-parser-image:v1.0.1
docker push orestklymko/my-parser-image:v1.0.1